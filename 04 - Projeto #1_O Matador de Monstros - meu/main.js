new Vue({
    el: '#app',
    data: {
        playerLife: 100,
        monsterLife: 100,
        playing: false,
        gameLogs: [],
    },
    computed: {},
    watch: {
        playerLife(newLife) {
            if (newLife <= 0) {
                this.playerLife = 0;
                this.playing = false;
            }
        },
        monsterLife(newLife) {
            if (newLife <= 0) {
                this.monsterLife = 0;
                this.playing = false;
            }
        },
    },
    methods: {
        clearGame() {
            this.playerLife = 100;
            this.monsterLife = 100;
            this.playing = false;
            this.gameLogs = [];
        },
        startGame() {
            this.clearGame();
            this.playing = true;
        },
        executeAction(action) {
            let playerAttack = 0;
            switch (action) {
                case 'A':
                    playerAttack = Math.ceil(Math.random() * 20);
                    this.monsterLife -= playerAttack;
                    this.gameLogs.push({
                        logStyle: 'alert-primary',
                        playerAttack,
                    });
                    break;
                case 'S':
                    playerAttack = Math.ceil(Math.random() * 30 + 10);
                    this.monsterLife -= playerAttack;
                    this.gameLogs.push({
                        logStyle: 'alert-primary',
                        playerAttack,
                    });
                    break;
                case 'H':
                    const heroHeal = Math.ceil(Math.random() * 20);
                    this.playerLife += heroHeal;
                    if (this.playerLife > 100) this.playerLife = 100;
                    this.gameLogs.push({
                        logStyle: 'alert-success',
                        heroHeal,
                    });
                    break;

                default:
                    break;
            }
            const monsterAttack = Math.ceil(Math.random() * 30);
            this.playerLife -= monsterAttack;
            this.gameLogs.push({
                logStyle: 'alert-danger',
                monsterAttack,
            });
        },
    },
});
