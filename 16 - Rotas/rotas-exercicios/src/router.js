import Vue from 'vue';
import VueRouter from 'vue-router';
import Inicio from './components/Inicio.vue';
import Menu from './components/template/Menu.vue';
import MenuAlt from './components/template/MenuAlt.vue';
// import Usuario from './components/usuario/Usuario.vue';
// import UsuarioLista from './components/usuario/UsuarioLista';
// import UsuarioDetalhe from './components/usuario/UsuarioDetalhe';
// import UsuarioEditar from "./components/usuario/UsuarioEditar";

Vue.use(VueRouter);

const Usuario = () =>
  import(/* webpackChunkName: "usuario" */ './components/usuario/Usuario');
const UsuarioLista = () =>
  import(/* webpackChunkName: "usuario" */ './components/usuario/UsuarioLista');
const UsuarioDetalhe = () => import('./components/usuario/UsuarioDetalhe');
const UsuarioEditar = () => import('./components/usuario/UsuarioEditar');

const router = new VueRouter({
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) return savedPosition;
    if (to.hash) return { selector: to.hash };
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: '/',
      // component: Inicio
      components: {
        default: Inicio,
        menu: Menu
      }
    },
    {
      path: '/usuario',
      // component: Usuario,
      components: {
        default: Usuario,
        menu: MenuAlt,
        menuInferior: MenuAlt
      },
      props: true,
      children: [
        { path: '', component: UsuarioLista, name: 'inicio' },
        {
          path: ':id',
          component: UsuarioDetalhe,
          props: true,
          beforeEnter: (to, from, next) => {
            next();
          }
        },
        {
          path: ':id/editar',
          component: UsuarioEditar,
          props: true,
          name: 'editarUsuario'
        }
      ]
    },
    {
      path: '/redirecionar',
      redirect: '/usuario'
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});

router.beforeEach((to, from, next) => {
  // if (to.path !== "/usuario") next("/usuario");
  // else next(false);
  next();
});

export default router;
