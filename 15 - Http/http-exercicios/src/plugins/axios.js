import Vue from "vue";
import axios from "axios";

// axios.defaults.baseURL = "https://curso-vue-4af4a.firebaseio.com/";
// axios.defaults.geaders.common['Authorization'] = "abc123";
// axios.defaults.geaders.get['Authorization'] = "application/json";

Vue.use({
    install(Vue) {
        // Vue.prototype.$http = axios;
        Vue.prototype.$http = axios.create({
            baseURL: "https://curso-vue-4af4a.firebaseio.com/",
            headers: {
                get: {
                    Authorization: "abc123"
                }
            }
        });

        Vue.prototype.$http.interceptors.request.use(
            config => config,
            error => Promise.reject(error)
        );

        Vue.prototype.$http.interceptors.response.use(
            res => {
                // const array = [];
                // for (const chave in res.data) {
                //     array.push({ id: chave, ...res.data[chave] });
                // }
                // res.data = array;

                return res;
            },
            error => Promise.reject(error)
        );
    }
});
