export default {
    computed: {
        fraseComVirgula() {
            return this.frase.toString().replace(/\s/g, ",");
        },
        fraseComTamanho() {
            return this.frase
                ? this.frase
                      .split(" ")
                      .map(p => `${p} (${p.length})`)
                      .join(" ")
                : "";
        }
    }
};
