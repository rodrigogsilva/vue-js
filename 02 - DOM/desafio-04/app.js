new Vue({
    el: '#desafio',
    data: {
        porcentagem: 0,
        c1: 'c1',
        efeito: '',
        classeCss: 'estilo1',
        classeCss4: 'estilo2',
        bgcolor: 'red',
        girar: false,
    },
    computed: {
        myStyle() {
            return {
                backgroundColor: 'orange',
                width: this.porcentagem + '%',
                height: '20px',
                borderRadius: '10px',
            };
        },
    },
    methods: {
        iniciarEfeito() {
            setInterval(() => {
                this.efeito =
                    this.efeito == 'destaque' ? 'encolher' : 'destaque';
            }, 1000);
        },
        iniciarProgresso() {
            const interval = setInterval(() => {
                if (this.porcentagem >= 100) {
                    clearInterval(interval);
                    return;
                }
                this.porcentagem++;
            }, 50);
        },
        setGirar(event) {
            const val = event.target.value;
            if (val == 'false') this.girar = false;
            else if (val == 'true') this.girar = true;
        },
    },
});
