new Vue({
    el: '#desafio',
    data: {
        valor: '',
    },
    methods: {
        mostraAlerta() {
            alert('Exibindo alerta!');
        },
        updateValor(event) {
            this.valor = event.target.value;
        },
    },
});
