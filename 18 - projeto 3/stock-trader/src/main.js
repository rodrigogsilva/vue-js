import Vue from 'vue'

import './plugins/vuetify'
import './plugins/axios'

import App from './App.vue'

import store from './store'
import router from './router'
import currencies from './filters/currencies'

Vue.config.productionTip = false

Vue.filter('currency', currencies)

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
