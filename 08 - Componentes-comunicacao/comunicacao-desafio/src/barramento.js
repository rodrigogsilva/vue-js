import Vue from "vue";
export default new Vue({
    methods: {
        enviaUsuario(usuario) {
            this.$emit("dadosUsuario", usuario);
        },
        recebeUsuario(callback) {
            this.$on("dadosUsuario", callback);
        }
    }
});
