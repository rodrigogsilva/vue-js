import Vue from "vue";
export default new Vue({
    methods: {
        alterarIdade(idade) {
            this.$emit("idadeNova", idade);
        },
        quandoIdadeMudar(callback) {
            this.$on("idadeNova", callback);
        }
    }
});
